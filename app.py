from flask import Flask, jsonify
import requests

app = Flask(__name__)

@app.route("/lyrics/<artist>/<songname>")
def love_bubi(artist, songname):
    url = f'https://www.google.com/search?q={artist}+{songname}+lyrics'
    r = requests.get(url)
    lyrics = r.text.split('<div class="BNeawe tAd8D AP7Wnd"><span dir="ltr">')[2].split('</div>')[0]
    lyricsArray = repr(lyrics).split('\\n')
    return jsonify(lyrics=lyricsArray)

@app.route("/")
def hello_world():
    return "<p>Hello, World !</p>"